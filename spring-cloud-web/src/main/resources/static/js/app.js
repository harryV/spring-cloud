angular.module('csts', ['ngRoute']).config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'cst-page.html',
        controller: 'baseController'
    })
}).controller('baseController', function ($scope, $http) {
    $http.get('queryList').success(function (data) {
        $scope.cstList = data;
    });
});