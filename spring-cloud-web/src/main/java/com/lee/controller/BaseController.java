package com.lee.controller;

import com.lee.model.CstBase;
import com.lee.service.CstBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lizhe
 */
@RestController
public class BaseController {

    @Autowired
    private CstBaseService cstBaseService;

//    @RequestMapping("/getClientCst")
//    public String getCst() {
//        return cstBaseService.getCst();
//    }

    @RequestMapping("/queryList")
    public List<CstBase> queryList() {
        return cstBaseService.queryListByPage();
    }
}
