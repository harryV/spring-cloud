package com.lee.service;


import com.lee.model.CstBase;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lizhe
 */
@Service
public class CstBaseService {

    @Autowired
    RestTemplate restTemplate;

    final String SERVICE_NAME = "spring-cloud-service";

//    @HystrixCommand(fallbackMethod = "fallbackSearchAll")
//    public String getCst() {
//        return restTemplate.getForObject("http://" + SERVICE_NAME + "/cst", String.class);
//    }

    @HystrixCommand(fallbackMethod = "fallbackSearchAll")
    public List<CstBase> queryListByPage() {
        return restTemplate.getForObject("http://" + SERVICE_NAME + "/queryListByPage", List.class);
    }

    private List<CstBase> fallbackSearchAll() {
        System.out.println("HystrixCommand fallbackMethod handle!");
        List<CstBase> list = new ArrayList<CstBase>();
        CstBase base = new CstBase();
        base.setName("TestHystrixCommand");
        list.add(base);
        return list;
    }
}
