package com.lee.mapper;


import com.lee.model.CstBase;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CstBaseMapper {

    CstBase selectByPrimaryKey(Long cstId);

    List<CstBase> queryListByPage();

}