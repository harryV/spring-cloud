package com.lee.service;


import com.lee.mapper.CstBaseMapper;
import com.lee.model.CstBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lizhe
 */
@Service
public class CstBaseService {

    @Autowired
    private CstBaseMapper mapper;

    public CstBase queryByCstId(long cstId) {
        return mapper.selectByPrimaryKey(cstId);
    }

    public List<CstBase> queryListByPage() {
        return mapper.queryListByPage();
    }
}
