package com.lee.controller;

import com.lee.model.CstBase;
import com.lee.service.CstBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lizhe
 */
@RestController
public class BaseController {
    @Autowired
    CstBaseService baseService;

    @RequestMapping(value = "/cst", method = RequestMethod.GET)
    public String getCst() {
        final CstBase cstBase = baseService.queryByCstId(10000000002L);
        return cstBase.getCstId() + "--" + cstBase.getName();
    }

    @RequestMapping(value = "/queryListByPage", method = RequestMethod.GET)
    public List<CstBase> queryListByPage() {
        return baseService.queryListByPage();
    }
}
