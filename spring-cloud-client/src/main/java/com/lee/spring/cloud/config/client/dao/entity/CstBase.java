package com.lee.spring.cloud.config.client.dao.entity;

import java.util.Date;

public class CstBase {
    private Long cstId;

    private String name;

    private String nickName;

    private String namePinyin;

    private String nicknamePinyin;

    private String firstNameLetter;

    private String firstNicknameLetter;

    private Integer registerWay;

    private Integer registerChanner;

    private Integer customerSource;

    private String developingPerson;

    private String hrEmployee;

    private Integer status;

    private Integer type;

    private Integer idType;

    private String idCode;

    private String phone;

    private String mail;

    private String businessArea;

    private String addressWork;

    private Integer provinceWork;

    private Integer cityWork;

    private Integer districtWork;

    private String level;

    private Integer category;

    private String directShopId;

    private Date signTime;

    private Integer signStatus;

    private Integer payType;

    private Boolean isDelete;

    private Integer version;

    private Date createTime;

    private String creater;

    private Date updateTime;

    private String modifier;

    private String cstLabel;

    public Long getCstId() {
        return cstId;
    }

    public void setCstId(Long cstId) {
        this.cstId = cstId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public String getNamePinyin() {
        return namePinyin;
    }

    public void setNamePinyin(String namePinyin) {
        this.namePinyin = namePinyin == null ? null : namePinyin.trim();
    }

    public String getNicknamePinyin() {
        return nicknamePinyin;
    }

    public void setNicknamePinyin(String nicknamePinyin) {
        this.nicknamePinyin = nicknamePinyin == null ? null : nicknamePinyin.trim();
    }

    public String getFirstNameLetter() {
        return firstNameLetter;
    }

    public void setFirstNameLetter(String firstNameLetter) {
        this.firstNameLetter = firstNameLetter == null ? null : firstNameLetter.trim();
    }

    public String getFirstNicknameLetter() {
        return firstNicknameLetter;
    }

    public void setFirstNicknameLetter(String firstNicknameLetter) {
        this.firstNicknameLetter = firstNicknameLetter == null ? null : firstNicknameLetter.trim();
    }

    public Integer getRegisterWay() {
        return registerWay;
    }

    public void setRegisterWay(Integer registerWay) {
        this.registerWay = registerWay;
    }

    public Integer getRegisterChanner() {
        return registerChanner;
    }

    public void setRegisterChanner(Integer registerChanner) {
        this.registerChanner = registerChanner;
    }

    public Integer getCustomerSource() {
        return customerSource;
    }

    public void setCustomerSource(Integer customerSource) {
        this.customerSource = customerSource;
    }

    public String getDevelopingPerson() {
        return developingPerson;
    }

    public void setDevelopingPerson(String developingPerson) {
        this.developingPerson = developingPerson == null ? null : developingPerson.trim();
    }

    public String getHrEmployee() {
        return hrEmployee;
    }

    public void setHrEmployee(String hrEmployee) {
        this.hrEmployee = hrEmployee == null ? null : hrEmployee.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIdType() {
        return idType;
    }

    public void setIdType(Integer idType) {
        this.idType = idType;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode == null ? null : idCode.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea == null ? null : businessArea.trim();
    }

    public String getAddressWork() {
        return addressWork;
    }

    public void setAddressWork(String addressWork) {
        this.addressWork = addressWork == null ? null : addressWork.trim();
    }

    public Integer getProvinceWork() {
        return provinceWork;
    }

    public void setProvinceWork(Integer provinceWork) {
        this.provinceWork = provinceWork;
    }

    public Integer getCityWork() {
        return cityWork;
    }

    public void setCityWork(Integer cityWork) {
        this.cityWork = cityWork;
    }

    public Integer getDistrictWork() {
        return districtWork;
    }

    public void setDistrictWork(Integer districtWork) {
        this.districtWork = districtWork;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level == null ? null : level.trim();
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getDirectShopId() {
        return directShopId;
    }

    public void setDirectShopId(String directShopId) {
        this.directShopId = directShopId == null ? null : directShopId.trim();
    }

    public Date getSignTime() {
        return signTime;
    }

    public void setSignTime(Date signTime) {
        this.signTime = signTime;
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater == null ? null : creater.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public String getCstLabel() {
        return cstLabel;
    }

    public void setCstLabel(String cstLabel) {
        this.cstLabel = cstLabel;
    }
}