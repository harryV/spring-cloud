package com.lee.spring.cloud.config.client.dao.mapper;


import com.lee.spring.cloud.config.client.dao.entity.CstBase;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CstBaseMapper {
    CstBase selectByPrimaryKey(Long cstId);

}