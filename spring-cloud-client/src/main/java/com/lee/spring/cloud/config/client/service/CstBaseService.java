package com.lee.spring.cloud.config.client.service;


import com.lee.spring.cloud.config.client.dao.entity.CstBase;

/**
 * @author lizhe
 */
public interface CstBaseService {

    CstBase queryByCstId(long cstId);
}
