package com.lee.spring.cloud.config.client.controller;

import com.lee.spring.cloud.config.client.dao.entity.CstBase;
import com.lee.spring.cloud.config.client.service.CstBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lizhe
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    private CstBaseService service;

    @RequestMapping(value = "/list")
    @ResponseBody
    public String list() {

        final CstBase cstBase = service.queryByCstId(10000000002L);
        return cstBase.getName() + "+" + cstBase.getCstId();
    }
}
