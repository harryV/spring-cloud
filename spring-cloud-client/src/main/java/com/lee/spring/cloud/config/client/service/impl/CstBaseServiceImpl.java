package com.lee.spring.cloud.config.client.service.impl;

import com.lee.spring.cloud.config.client.dao.entity.CstBase;
import com.lee.spring.cloud.config.client.dao.mapper.CstBaseMapper;
import com.lee.spring.cloud.config.client.service.CstBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lizhe
 */
@Service
public class CstBaseServiceImpl implements CstBaseService {

    @Autowired
    private CstBaseMapper mapper;

    @Override
    public CstBase queryByCstId(long cstId) {
        return mapper.selectByPrimaryKey(cstId);
    }
}
